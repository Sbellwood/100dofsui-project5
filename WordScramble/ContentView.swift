//
//  ContentView.swift
//  WordScramble
//
//  Created by Skyler Bellwood on 7/5/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State private var usedWords = [String]()
    @State private var allWords = [String]()
    @State private var rootWord = ""
    @State private var newWord = ""
    @State private var score = 0
    
    @State private var errorTitle = ""
    @State private var errorMessage = ""
    @State private var showingError = false
    
    var body: some View {
        NavigationView {
            VStack {
                TextField("Enter your word", text: $newWord, onCommit: addNewWord)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding()
                    .autocapitalization(.none)
                
                List(usedWords, id: \.self) {
                    Image(systemName: "\(self.letterToScore(characterCount: $0.count)).circle")
                    Text($0)
                }
                Text("Score: \(score)")
            }
            .navigationBarTitle(rootWord)
            .navigationBarItems(leading: Button("New Word", action: startGame))
        }
        .onAppear(perform: loadWords)
        .alert(isPresented: $showingError) {
            Alert(title: Text(errorTitle), message: Text(errorMessage), dismissButton: .default(Text("OK")))
        }
    }
    
    func loadWords() {
        if let startWordsURL = Bundle.main.url(forResource: "start", withExtension: "txt") {
            if let startWords = try? String(contentsOf: startWordsURL) {
                allWords = startWords.components(separatedBy: "\n")
                startGame()
                return
            }
        }
        
        fatalError("Could not load start.txt from bundle")
    }
    
    func startGame() {
        usedWords.removeAll()
        score = 0
        rootWord = allWords.randomElement() ?? "silkworm"
    }
    
    func addNewWord() {
        let answer = newWord.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        
        guard check(answer) else {
            return
        }
        
        score += letterToScore(characterCount: answer.count)
        
        usedWords.insert(answer, at: 0)
        newWord = ""
    }
    
    func letterToScore(characterCount: Int) -> Int {
        switch characterCount {
        case 3:
            return 1
        case 4:
            return 2
        case 5:
            return 3
        case 6:
            return 5
        case 7:
            return 10
        default:
            return 0
        }
    }
    
    func isOriginal(word: String) -> Bool {
        !usedWords.contains(word)
    }
    
    func isPossible(word: String) -> Bool {
        var tempWord = rootWord
        
        for letter in word {
            if let pos = tempWord.firstIndex(of: letter) {
                tempWord.remove(at: pos)
            } else {
                return false
            }
        }
        
        return true
    }
    
    func isReal(word: String) -> Bool {
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count)
        let misspelledRange = checker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")
        
        return misspelledRange.location == NSNotFound
    }
    
    func isRootWord(word: String) -> Bool {
        return word == rootWord
    }
    
    func isSufficientlyLong(word: String) -> Bool {
        return word.count > 2
    }
    
    func check(_ answer: String) -> Bool {
        guard answer.count > 0 else {
            wordError(title: "No word typed", message: "Please type a word and then submit")
            return false
        }
        
        guard !isRootWord(word: answer) else {
            wordError(title: "That's the root word!", message: "That's cheating!")
            return false
        }
        
        guard isSufficientlyLong(word: answer) else {
            wordError(title: "Word too short", message: "Try to come up with a longer word.")
            return false
        }
        
        guard isOriginal(word: answer) else {
            wordError(title: "Word used already", message: "Be more original")
            return false
        }
        
        guard isPossible(word: answer) else {
            wordError(title: "Word not recognized", message: "You can't just make them up, you know!")
            return false
        }
        
        guard isReal(word: answer) else {
            wordError(title: "Word not possible", message: "That isn't a real world.")
            return false
        }
        
        return true
    }
    
    func wordError(title: String, message: String) {
        newWord = ""
        errorTitle = title
        errorMessage = message
        showingError = true
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environment(\.colorScheme, .dark)
    }
}
